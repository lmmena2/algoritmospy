import time
import sys

def factorial(n):
    respuesta = 1
    
    while n > 1:
        respuesta = respuesta * n
        n = n - 1
        
    return respuesta

def factorial_recursivo(n):

    if n == 1: 
        return 1
    
    return n * factorial_recursivo(n - 1)

if __name__ == '__main__':
    n = 200000
    
    sys.setrecursionlimit(1000000)
    
    comienzo_f = time.time()
    factorial(n)
    final_f = time.time()
    print (final_f - comienzo_f)
    
    comienzo_fr = time.time()
    factorial_recursivo(n)
    final_fr = time.time()
    print (final_fr - comienzo_fr)