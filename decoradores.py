def funcion_decoradora(funcion):
    
    #*args sirve para resirvir cualquier cantidad de parametros//**kwargs para pasar parametros con key 
    def funcion_interna(*args,**kwargs):
        print ("Incio de funcion decoradora")
        funcion(*args,**kwargs)
        print ("Fin de funcion decoradora")
    
    return funcion_interna

@funcion_decoradora
def suma(num1,num2,num3):
    
    print(num1+num2+num3)

@funcion_decoradora 
def resta(num1,num2):
    
    print(num1-num2)

@funcion_decoradora    
def potencia(base,exponente):
    print(pow(base,exponente))
   
   
suma(10,10,5)
resta(20,9)  
potencia(base=5,exponente=2)
       